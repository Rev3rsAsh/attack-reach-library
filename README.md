![Game version](https://img.shields.io/badge/Game%20version-1.21-yellowgreen?style=for-the-badge)
![Mod loader](https://img.shields.io/badge/Mod%20loader-Fabric-important?style=for-the-badge)
![License](https://img.shields.io/badge/License-CPL-informational?style=for-the-badge)

### Why does it exist?

The main reason is default hardcoding of attribute modifiers on items. Attribute modifiers are stored either in an immutable data component or immutable fields in the class, which are very hard to mixin into. The data component is created with a static function, during an item's creation. That means, these modifiers cannot be edited per-item (item argument is not passed, due to the function being static an item instance cannot be invoked and the function is invoked before the item even exists).

This library works around these issues by processing the attribute modifiers from the per-item list of ItemAttributeModifier entries, which can be added to that list by using the helper functions in the main class. This allows adding attribute modifiers for different items, attributes and target equipment slot configurations - all independently of each other.


### How to independently modify the items?

It should be done through an ItemStack instance (or a ItemStack mixin). Otherwise, due to how the helper functions are implemented, all item instance-based checks will fail because the item does not exist yet.



### Mod-development usage

Add the maven of the library to your build.gradle file:

```
repositories {
    ...
    maven {
        url "https://gitlab.com/api/v4/projects/56272048/packages/maven"
    }
}
```

and then implement the library in the dependencies section of the same file:

```
dependencies {
    ...
    modImplementation "com.Rev3rsAsh:item_modifiers_library:<version>"
}
```
Where version is the desired version of the modification (for example: 1.20.4.1).

### End-user usage

Access [the package registry](https://gitlab.com/Rev3rsAsh/attributes-library/-/packages), choose desired version and download the .jar file (not the sources one).
Then, place the downloaded file in /mods folder located in the desired Minecraft instance's folder.

