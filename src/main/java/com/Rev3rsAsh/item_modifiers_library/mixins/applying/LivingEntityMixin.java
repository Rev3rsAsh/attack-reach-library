package com.Rev3rsAsh.item_modifiers_library.mixins.applying;

import com.Rev3rsAsh.item_modifiers_library.ItemModifiers;
import com.Rev3rsAsh.item_modifiers_library.api.ItemModifierEntry;
import com.llamalad7.mixinextras.sugar.Local;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.*;
import net.minecraft.world.item.*;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Map;

/** When a player changes the main hand item, the game reads values from the item's unchangeable attributes map.
 * If per-item attribute modifier value fields are to be used, these values must be overwritten.
 * This mixin reapplies attribute modifiers, for which per-item fields are implemented, to make sure that per-item values are used instead of unchangeable values from the attributes map. */
@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin {
    @Shadow @Final
    private AttributeMap attributes;
    @Unique
    final LivingEntity livingEntity = (LivingEntity) (Object) this;

    /** Detect if there are any changes in the LivingEntity's equipment slots.
     * If there are, remove attribute modifiers of previous ItemStacks and add attribute modifiers of ItemStacks currently in these slots. */
    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/world/item/ItemStack;isEmpty()Z", ordinal = 0, shift = At.Shift.AFTER), method = "collectEquipmentChanges")
    private void updatePlayerAttributesFromItem(CallbackInfoReturnable<Map<EquipmentSlot, ItemStack>> cir, @Local(ordinal = 0) ItemStack oldItemStack, @Local(ordinal = 1) ItemStack newItemStack, @Local(ordinal = 0) EquipmentSlot equipmentSlot) {
        // Do not process on the client, the server will update the client instead.
        if (!(livingEntity.getCommandSenderWorld() instanceof ServerLevel))
            return;
        // Get equipmentSlotGroup for processed equipmentSlot.
        EquipmentSlotGroup equipmentSlotGroup = EquipmentSlotGroup.bySlot(equipmentSlot);
        AttributeInstance attributeInstance;
        // Remove attribute modifiers of an ItemStack, which was in this slot before the change.
        for (ItemModifierEntry entry : ItemModifiers.getItemAttributeModifiers(oldItemStack.getItem())) {
            attributeInstance = this.attributes.getInstance(entry.attribute());
            if (attributeInstance != null && entry.slot().equals(equipmentSlotGroup))
                attributeInstance.removeModifier(entry.id());
        }
        AttributeModifier attributeModifier;
        // Add attribute modifiers of an ItemStack, which is now in this slot.
        for (ItemModifierEntry entry : ItemModifiers.getItemAttributeModifiers(newItemStack.getItem())) {
            attributeInstance = this.attributes.getInstance(entry.attribute());
            if (attributeInstance != null && entry.slot().equals(equipmentSlotGroup)) {
                attributeModifier = new AttributeModifier(entry.id(), entry.amount(), entry.operation());
                attributeInstance.addOrUpdateTransientModifier(attributeModifier);
            }
        }
    }
}
