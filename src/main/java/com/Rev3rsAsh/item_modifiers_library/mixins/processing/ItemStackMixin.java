package com.Rev3rsAsh.item_modifiers_library.mixins.processing;

import com.Rev3rsAsh.item_modifiers_library.ItemModifiers;
import com.Rev3rsAsh.item_modifiers_library.api.ItemModifierEntry;
import com.llamalad7.mixinextras.sugar.Local;
import net.minecraft.ChatFormatting;
import net.minecraft.core.Holder;
import net.minecraft.network.chat.CommonComponents;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.function.Consumer;

/** For most items, the game now uses the data component system (defining item properties in JSON files).
 * While it is convenient, it makes changing attribute modifiers of an item very hard without mod-compatibility-breaking overwrites.
 * Also, changes in-code made before the data component is compiled into an immutable list cannot use Item instances (no changes based on item type are possible) due to the function being static and the item instance not being passed as an argument.
 * This class contains code required by this library to work with ItemStacks, along with low-level implementations of per-ItemStack code, used by this library to store these modifiers in an easily accessible set.
 * This set is used in place of the data component (and hardcoded lists), which allows the mod developers to freely and safely add/modify/remove attribute modifiers of ItemStacks, even after an ItemStack is already created.*/
@Mixin(ItemStack.class)
public abstract class ItemStackMixin{
    @Shadow
    public abstract Item getItem();

    /** Cancel default processing of tooltip lines. */
    @Inject(at = @At(value = "HEAD"), method = "addModifierTooltip", cancellable = true)
    private void cancelDefaultAttributeLines(Consumer<Component> consumer, Player player, Holder<Attribute> holder, AttributeModifier attributeModifier, CallbackInfo ci) {
        // This library already processes all attributes with any attribute modifiers, for all EquipmentSlotGroups.
        // Vanilla code, which adds attribute modifier tooltip lines, is not desired to run because it would create duplicated lines in the ItemStack's tooltip.
        ci.cancel();
    }

    /** Get all modified attributes of an itemStack, and prepare to reimplement them in order using the designated method. */
    @Inject(at = @At(value = "INVOKE", target = "Lnet/minecraft/world/item/ItemStack;forEachModifier(Lnet/minecraft/world/entity/EquipmentSlotGroup;Ljava/util/function/BiConsumer;)V", shift = At.Shift.AFTER), method = "addAttributeTooltips")
    private void addSupportedAttributesLines(Consumer<Component> consumer, @Nullable Player player, CallbackInfo ci, @Local EquipmentSlotGroup equipmentSlotGroup) {
        // Process each attribute, which has any attribute modifiers on an ItemStack.
        for (Holder<Attribute> attributeHolder : ItemModifiers.getItemModifiedAttributes(this.getItem(), equipmentSlotGroup)) {
            // Pass the parameters to the method, which really creates the tooltip lines.
            addAttributeTooltipLine(attributeHolder, equipmentSlotGroup, consumer, player);
        }
    }

    /** Reimplement the attribute modifiers tooltip lines, for each attribute containing any modifiers on the ItemStack, with the attribute value format. */
    @Unique
    private void addAttributeTooltipLine(Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup, Consumer<Component> consumer, Player player) {
        // Initialise the variable, which stores the attribute's total value.
        double totalValue = 0.0;
        // Get all attribute modifiers of the attribute, for provided equipmentSlotGroup.
        LinkedHashSet<ItemModifierEntry> attributeModifiers = ItemModifiers.getItemAttributeModifiers(this.getItem(), attributeHolder, equipmentSlotGroup);
        // If there are no attribute modifiers, assume that showing the attribute's tooltip line is not desired and stop its generation.
        if (attributeModifiers.isEmpty())
            return;
        // Player-specific modifications can be made only if provided player exists.
        if (player != null) {
            // Add the attribute's base value to the total value variable.
            totalValue = player.getAttributeBaseValue(attributeHolder);
            // If the processed attribute is the attack reach, try to add creative mode bonus to the totalValue.
            if (attributeHolder.equals(Attributes.ENTITY_INTERACTION_RANGE)) {
                ResourceLocation creativeModeAttackReachBonus = ResourceLocation.withDefaultNamespace("creative_mode_entity_range");
                AttributeInstance attackReachInstance = player.getAttribute(Attributes.ENTITY_INTERACTION_RANGE);
                if (attackReachInstance != null && attackReachInstance.hasModifier(creativeModeAttackReachBonus))
                    // Add the creative mode bonus to the attack reach tooltip line.
                    totalValue += attackReachInstance.getModifier(creativeModeAttackReachBonus).amount();
            }
        }
        // Initialise the variable, which stores the total multiplier of all attribute modifiers with the ADD_MULTIPLIED_BASE operation.
        double multiplyBaseValue = 0.0;
        // Initialise the list, which stores multiplier values of all attribute modifiers with the ADD_MULTIPLIED_TOTAL operation.
        LinkedList<Double> multiplyTotalModifiers = new LinkedList<>();
        // Add the amount of each applicable attribute modifier to the proper variable.
        for (ItemModifierEntry entry : attributeModifiers) {
            switch (entry.operation()) {
                case ADD_VALUE -> totalValue += entry.amount();
                case ADD_MULTIPLIED_BASE -> multiplyBaseValue += entry.amount();
                case ADD_MULTIPLIED_TOTAL -> multiplyTotalModifiers.add(entry.amount());
            }
        }
        // Execute the ADD_MULTIPLIED_BASE operation.
        totalValue *= 1 + multiplyBaseValue;
        // Execute the ADD_MULTIPLIED_TOTAL operation.
        for (double doubleValue : multiplyTotalModifiers) {
            totalValue *= 1 + doubleValue;
        }
        // If the totalValue of the attribute still equals 0.0, then there is no point to create its tooltip line.
        if (totalValue != 0.0) {
            // Clamp the totalValue in the attribute's min/max range - the attribute can only have a value in this range anyway.
            totalValue = attributeHolder.value().sanitizeValue(totalValue);
            // Finally, add the attribute's line to the item's tooltip.
            consumer.accept(CommonComponents.space().append(Component.translatable("attribute.modifier.equals." + AttributeModifier.Operation.ADD_VALUE.id(), ItemAttributeModifiers.ATTRIBUTE_MODIFIER_FORMAT.format(totalValue), Component.translatable(attributeHolder.value().getDescriptionId()))).withStyle(ChatFormatting.DARK_GREEN));
        }
    }
}
