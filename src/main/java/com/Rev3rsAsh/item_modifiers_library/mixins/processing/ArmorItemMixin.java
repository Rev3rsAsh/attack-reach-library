package com.Rev3rsAsh.item_modifiers_library.mixins.processing;

import com.Rev3rsAsh.item_modifiers_library.api.ItemModifiersHelper;
import net.minecraft.core.Holder;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Supplier;

@Mixin(ArmorItem.class)
public abstract class ArmorItemMixin implements ItemModifiersHelper {
    @Shadow @Final private Supplier<ItemAttributeModifiers> defaultModifiers;

    /** Add hardcoded attribute modifiers to the armor piece item's attribute modifiers set. */
    @Inject(at = @At(value = "TAIL"), method = "<init>")
    private void storeDefaultAttributeModifiersValues(Holder<ArmorMaterial> holder, ArmorItem.Type type, Item.Properties properties, CallbackInfo ci) {
        // In this version of the game, armor piece items still use a hardcoded list of attribute modifiers instead of the data component - add them to the modifiers set.
        for (ItemAttributeModifiers.Entry itemAttributeModifier : this.defaultModifiers.get().modifiers()) {
            itemModifiersLibrary$addItemAttributeModifier(itemAttributeModifier.attribute(), itemAttributeModifier.modifier().id(), itemAttributeModifier.modifier().amount(), itemAttributeModifier.modifier().operation(), itemAttributeModifier.slot());
        }
    }
}
