package com.Rev3rsAsh.item_modifiers_library.mixins.processing;

import com.Rev3rsAsh.item_modifiers_library.ItemModifiers;
import com.Rev3rsAsh.item_modifiers_library.api.ItemModifierEntry;
import com.Rev3rsAsh.item_modifiers_library.api.ItemModifiersHelper;
import net.minecraft.core.Holder;
import net.minecraft.core.component.DataComponentMap;
import net.minecraft.core.component.DataComponents;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.*;
import net.minecraft.world.item.component.ItemAttributeModifiers;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.LinkedHashSet;
import java.util.List;

// TODO: switch from equipmentSlotGroup to equipmentSlot? - prevent intentional breakages
/** For most items, the game now uses the data component system (defining item properties in JSON files).
 * While it is convenient, it makes changing attribute modifiers of an item very hard without mod-compatibility-breaking overwrites.
 * Also, changes in-code made before the data component is compiled into an immutable list cannot use Item instances (no changes based on item type are possible) due to the function being static and the item instance not being passed as an argument.
 * This class contains code required by this library to store these modifiers in an easily accessible set.
 * This set is used in place of the data component (and hardcoded lists), which allows the mod developers to freely and safely add/modify/remove attribute modifiers of items.*/
@Mixin(value = Item.class, priority = 500)
public abstract class ItemMixin implements ItemModifiersHelper {
    @Shadow @Final
    private DataComponentMap components;

    @Shadow @Deprecated public abstract ItemAttributeModifiers getDefaultAttributeModifiers();

    @Unique
    Item item = (Item) (Object) this;

    /** Add data component and hardcoded attribute modifiers to the item's attribute modifiers set. */
    @Inject(at = @At(value = "TAIL"), method = "<init>")
    private void storeDefaultAttributeModifiersValues(Item.Properties properties, CallbackInfo ci) {
        // Acquire the attribute modifiers from the data component.
        // Data component should be present on all items by default.
        if (this.components.has(DataComponents.ATTRIBUTE_MODIFIERS)) {
            List<ItemAttributeModifiers.Entry> componentAttributeModifiers = this.components.getOrDefault(DataComponents.ATTRIBUTE_MODIFIERS, ItemAttributeModifiers.EMPTY).modifiers();
            if (!componentAttributeModifiers.isEmpty()) {
                // Do not worry about the warning in here, null data will never be processed due to the check above.
                for (ItemAttributeModifiers.Entry itemAttributeModifier : componentAttributeModifiers) {
                    itemModifiersLibrary$addItemAttributeModifier(itemAttributeModifier.attribute(), itemAttributeModifier.modifier().id(), itemAttributeModifier.modifier().amount(), itemAttributeModifier.modifier().operation(), itemAttributeModifier.slot());
                }
            }
        }
        // Add no-value attack reach attribute modifier, just so the tooltip line can be displayed later.
        if (item instanceof TieredItem || item instanceof TridentItem || item instanceof MaceItem)
            itemModifiersLibrary$addItemAttributeModifier(Attributes.ENTITY_INTERACTION_RANGE, ResourceLocation.fromNamespaceAndPath(ItemModifiers.MOD_ID, "attack_reach_tooltip"), 0.0, AttributeModifier.Operation.ADD_VALUE, EquipmentSlotGroup.MAINHAND);
    }

    /** This set stores all the item's attribute modifiers.
     * Modifiers from the item's attribute modifiers data component and hardcoded list (if exists) are automatically added to this set by the library.
     * Also, it stores all modifiers added by the mod developers using the helper functions in the main class.
     * It uses the LinkedHashSet implementation to keep order of vanilla modifiers (and make adding attribute modifiers more performant). */
    @Unique
    LinkedHashSet<ItemModifierEntry> attributeModifiers = new LinkedHashSet<>();

    /** Internal implementation of the addItemAttributeModifier helper function. */
    @Override
    public void itemModifiersLibrary$addItemAttributeModifier(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup) {
        // Add the attribute modifier, for provided parameters, to the attribute modifiers set.
        attributeModifiers.add(new ItemModifierEntry(attributeHolder, attributeModifierId, amount, operation, equipmentSlotGroup));
    }

    /** Internal implementation of the modifyItemAttributeModifierAmount helper function. */
    @Override
    public boolean itemModifiersLibrary$modifyItemAttributeModifierAmount(Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup, ResourceLocation attributeModifierId, double amount) {
        for (ItemModifierEntry entry : attributeModifiers) {
            if (entry.attribute().equals(attributeHolder) && entry.slot().equals(equipmentSlotGroup) && entry.is(attributeModifierId)) {
                entry.setAmount(amount);
                return true;
            }
        } return false;
    }

    /** Internal implementation of the addItemAttributeModifierOrModifyAmount helper function. */
    @Override
    public void itemModifiersLibrary$addItemAttributeModifierOrModifyAmount(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup) {
        if (!itemModifiersLibrary$modifyItemAttributeModifierAmount(attributeHolder, equipmentSlotGroup, attributeModifierId, amount))
            itemModifiersLibrary$addItemAttributeModifier(attributeHolder, attributeModifierId, amount, operation, equipmentSlotGroup);
    }

    /** Internal implementation of the removeItemAttributeModifier helper function. */
    @Override
    public void itemModifiersLibrary$removeItemAttributeModifier(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, EquipmentSlotGroup equipmentSlotGroup) {
        ItemModifierEntry entryToRemove = null;
        // Search through all the attribute modifiers in the set to remove an attribute modifier matching provided attributeModifierId and equipmentSlotGroup.
        for (ItemModifierEntry entry : attributeModifiers) {
            if (entry.attribute().equals(attributeHolder) && entry.is(attributeModifierId) && entry.slot().equals(equipmentSlotGroup)) {
                // Make the entryToRemove variable point to the for-removal object.
                entryToRemove = entry;
                // There can be only one attribute modifier with provided attributeModifierId and equipmentSlotGroup - further iteration is not needed.
                break;
            }
        }
        if (entryToRemove != null) {
            // Remove the attribute modifier, which is matching provided parameters.
            attributeModifiers.remove(entryToRemove);
        }
    }

    /** Internal implementation of the getItemModifiedAttributes helper function. */
    @Override
    public LinkedHashSet<Holder<Attribute>> itemModifiersLibrary$getItemModifiedAttributes(EquipmentSlotGroup equipmentSlotGroup) {
        // Create a new LinkedHashSet, containing all found attributes which have attribute modifiers for provided equipmentSlotGroup.
        LinkedHashSet<Holder<Attribute>> attributeList = new LinkedHashSet<>();
        // Search through all the attribute modifiers in the set to find attribute modifiers for provided equipmentSlotGroup.
        for (ItemModifierEntry entry : attributeModifiers) {
            // If the attribute modifier's slot matches provided equipmentSlotGroup, add its attribute to the created LinkedHashSet.
            if (entry.slot().equals(equipmentSlotGroup)) {
                attributeList.add(entry.attribute());
            }
        }
        // Return the LinkedHashSet of all attributes, which have any attribute modifiers for provided equipmentSlotGroup.
        return attributeList;
    }

    /** Internal implementation of a getItemModifiedAttributes helper function. */
    @Override
    public LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers(Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup) {
        // Create a new LinkedHashSet, containing all found attribute modifiers for provided attributeHolder and equipmentSlotGroup.
        LinkedHashSet<ItemModifierEntry> list = new LinkedHashSet<>();
        // Search through all the attribute modifiers in the set to find attribute modifiers for provided attributeHolder and equipmentSlotGroup.
        for (ItemModifierEntry entry : attributeModifiers) {
            // If the attribute modifier's attribute matches provided attributeHolder and slot matches equipmentSlotGroup, add it to the created LinkedHashSet.
            if (entry.attribute().equals(attributeHolder) && entry.slot().equals(equipmentSlotGroup)) {
                list.add(entry);
            }
        }
        // Return the LinkedHashSet of all attribute modifiers for provided attributeHolder and equipmentSlotGroup.
        return list;
    }

    /** Internal implementation of a getItemModifiedAttributes helper function. */
    @Override
    public LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers(Holder<Attribute> attributeHolder) {
        // Create a new LinkedHashSet, containing all found attribute modifiers for provided attributeHolder.
        LinkedHashSet<ItemModifierEntry> list = new LinkedHashSet<>();
        // Search through all the attribute modifiers in the set to find attribute modifiers for provided attributeHolder.
        for (ItemModifierEntry entry : attributeModifiers) {
            // If the attribute modifier's attribute matches provided attributeHolder, add it to the created LinkedHashSet.
            if (entry.attribute().equals(attributeHolder)) {
                list.add(entry);
            }
        }
        // Return the LinkedHashSet of all attribute modifiers for provided attributeHolder.
        return list;
    }

    /** Internal implementation of a getItemModifiedAttributes helper function. */
    @Override
    public LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers() {
        // When no parameters are provided, return the LinkedHashSet of all attribute modifiers of the item.
        return attributeModifiers;
    }
}
