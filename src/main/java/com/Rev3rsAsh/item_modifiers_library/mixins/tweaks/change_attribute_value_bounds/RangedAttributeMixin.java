package com.Rev3rsAsh.item_modifiers_library.mixins.tweaks.change_attribute_value_bounds;

import net.minecraft.world.entity.ai.attributes.RangedAttribute;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(RangedAttribute.class)
public abstract class RangedAttributeMixin {
    @Shadow @Final @Mutable
    private double minValue;

    /** Changes minimum and/or maximum values of attributes. */
    @Inject(at = @At(value = "TAIL"), method = "<init>")
    private void storeAttributeId(String string, double d, double e, double f, CallbackInfo ci) {
        // Entity Interaction Range
        if (string.equals("attribute.name.player.entity_interaction_range")) {
            // The default minimum value of entity interaction range attribute is too high to allow for balancing adjustments around items, if they are needed.
            this.minValue = 0.5D;
        }
    }
}
