package com.Rev3rsAsh.item_modifiers_library.mixins.tweaks.sync_all_attributes;

import net.minecraft.world.entity.ai.attributes.Attribute;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Attribute.class)
public abstract class AttributeMixin {
    @Shadow
    public abstract Attribute setSyncable(boolean bl);

    /** Fixes some attributes (like attack damage) having different values on client and server, when the base value is modified, due to lack of server-to-client sync on them.
     * For example, lack of synchronisation on attack damage makes items show incorrect item attack damage values, if the base value is changed.
     * This call simply makes every attribute syncable, so the server will update the client with the current value of an attribute when a change is detected. */
    @Inject(at = @At(value = "TAIL"), method = "<init>")
    private void syncClientAndServerAttributeValues(String string, double d, CallbackInfo ci) {
        this.setSyncable(true);
    }
}
