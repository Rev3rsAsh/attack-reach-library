package com.Rev3rsAsh.item_modifiers_library;

import com.Rev3rsAsh.item_modifiers_library.api.ItemModifierEntry;
import com.Rev3rsAsh.item_modifiers_library.api.ItemModifiersHelper;
import net.fabricmc.api.ModInitializer;
import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;

// TODO: check if all nice-to-have functions are implemented
// TODO: do not process the modifiers maps at all if ItemStack's count > 1 - or do not let merging ItemStacks with non-empty-modifiers instances
// TODO: process the default modifiers but do not process external attribute modifiers if an item was created with a set component with commands
// TODO: add support for effects and enchantments in item tooltip values (for example: strength and sharpness do not update the attack damage value in tooltips of items)
/** Main class of this mod, containing helper functions which should be used by mod developers to get or modify per-item attribute modifiers values. */
@SuppressWarnings("unused")
public class ItemModifiers implements ModInitializer {
    public static final String MOD_ID = "item_modifiers_library";
    public static final Logger MOD_LOGGER = LoggerFactory.getLogger(MOD_ID);

    @Override
    public void onInitialize() {}

    /** Returns provided player's current value of the Attack Reach attribute. */
    public static double getPlayerAttackReach(Player player) {
        return player.getAttributeValue(Attributes.ENTITY_INTERACTION_RANGE);
    }

    /** Returns set of all attribute modifiers of an item, for provided attributeHolder and equipmentSlotGroup. */
    public static LinkedHashSet<ItemModifierEntry> getItemAttributeModifiers(Item item, Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup) {
        return ((ItemModifiersHelper) item).itemModifiersLibrary$getItemAttributeModifiers(attributeHolder, equipmentSlotGroup);
    }

    /** Returns set of all attribute modifiers of an item, for provided attributeHolder. */
    public static LinkedHashSet<ItemModifierEntry> getItemAttributeModifiers(Item item, Holder<Attribute> attributeHolder) {
        return ((ItemModifiersHelper) item).itemModifiersLibrary$getItemAttributeModifiers(attributeHolder);
    }

    /** Returns set of all attribute modifiers of an item. */
    public static LinkedHashSet<ItemModifierEntry> getItemAttributeModifiers(Item item) {
        return ((ItemModifiersHelper) item).itemModifiersLibrary$getItemAttributeModifiers();
    }

    /** Returns set of all attributes of an item, for provided equipmentSlotGroup, which have any attribute modifiers. */
    public static LinkedHashSet<Holder<Attribute>> getItemModifiedAttributes(Item item, EquipmentSlotGroup equipmentSlotGroup) {
        return ((ItemModifiersHelper) item).itemModifiersLibrary$getItemModifiedAttributes(equipmentSlotGroup);
    }

    /** Adds provided attribute modifier from provided attributeModifierId, amount and operation, for provided attributeHolder and equipmentSlotGroup, to the item's attribute modifiers set.
     * If you are sure that the modifier will not be duplicated, use this function because it is faster than addOrUpdateItemAttributeModifier. */
    public static void addItemAttributeModifier(Item item, Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup) {
        ((ItemModifiersHelper) item).itemModifiersLibrary$addItemAttributeModifier(attributeHolder, attributeModifierId, amount, operation, equipmentSlotGroup);
    }

    /** Adds provided attributeModifier, for provided attributeHolder and equipmentSlotGroup, to the item's attribute modifiers set.
     * If you are sure that the modifier will not be duplicated, use this function because it is faster than addOrUpdateItemAttributeModifier. */
    public static void addItemAttributeModifierOrModifyAmount(Item item, Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup) {
        ((ItemModifiersHelper) item).itemModifiersLibrary$addItemAttributeModifierOrModifyAmount(attributeHolder, attributeModifierId, amount, operation, equipmentSlotGroup);
    }

    /** Modifies amount of the attribute modifier matching provided attributeModifierId, for provided attributeHolder and equipmentSlotGroup.
     * The return value is an indicator, whether the matching attribute modifier has been found and its amount was modified.*/
    public static boolean modifyItemAttributeModifierAmount(Item item, Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup, ResourceLocation attributeModifierId, double amount) {
        return ((ItemModifiersHelper) item).itemModifiersLibrary$modifyItemAttributeModifierAmount(attributeHolder, equipmentSlotGroup, attributeModifierId, amount);
    }

    /** Removes the attribute modifier from the item's attribute modifiers set, for provided attributeModifierId and equipmentSlotGroup, if it exists. */
    public static void removeItemAttributeModifier(Item item, Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, EquipmentSlotGroup equipmentSlotGroup) {
        ((ItemModifiersHelper) item).itemModifiersLibrary$removeItemAttributeModifier(attributeHolder, attributeModifierId, equipmentSlotGroup);
    }

    /** Returns whether the provided attributeModifier is in the provided item's attribute modifiers set, for provided attributeEntry and equipmentSlotGroup. */
    public static boolean hasItemAttributeModifier(Item item, Holder<Attribute> attributeHolder, AttributeModifier attributeModifier, EquipmentSlotGroup equipmentSlotGroup) {
        // Iterate through all attribute modifiers, for provided attributeHolder and equipmentSlotGroup.
        for (ItemModifierEntry entry : ItemModifiers.getItemAttributeModifiers(item, attributeHolder, equipmentSlotGroup)) {
            // If the attribute modifier entry's id matches id of provided attributeModifier, it means that the attributeModifier already exists in the itemStack's attribute modifier set.
            if (entry.is(attributeModifier.id()))
                return true;
        }
        // The attributeModifier, for provided attributeHolder and equipmentSlotGroup, does not exist in the itemStack's attribute modifiers set.
        return false;
    }
}
