package com.Rev3rsAsh.item_modifiers_library.api;

import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;

import java.util.LinkedHashSet;

/** INTERNAL-ONLY interface, which implements a backbone for main attribute modifiers logic of this library.
 * It should not be implemented explicitly! Use the helper functions in the AttributesLibrary class to make item modifications instead. */
public interface ItemModifiersHelper {
    void itemModifiersLibrary$addItemAttributeModifier(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup);
    void itemModifiersLibrary$addItemAttributeModifierOrModifyAmount(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup equipmentSlotGroup);
    void itemModifiersLibrary$removeItemAttributeModifier(Holder<Attribute> attributeHolder, ResourceLocation attributeModifierId, EquipmentSlotGroup equipmentSlotGroup);

    boolean itemModifiersLibrary$modifyItemAttributeModifierAmount(Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup, ResourceLocation attributeModifierId, double amount);

    LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers(Holder<Attribute> attributeHolder, EquipmentSlotGroup equipmentSlotGroup);
    LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers(Holder<Attribute> attributeHolder);
    LinkedHashSet<ItemModifierEntry> itemModifiersLibrary$getItemAttributeModifiers();

    LinkedHashSet<Holder<Attribute>> itemModifiersLibrary$getItemModifiedAttributes(EquipmentSlotGroup equipmentSlotGroup);
}
