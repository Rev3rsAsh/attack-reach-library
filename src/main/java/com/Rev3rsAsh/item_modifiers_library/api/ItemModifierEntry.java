package com.Rev3rsAsh.item_modifiers_library.api;

import net.minecraft.core.Holder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;

public final class ItemModifierEntry {
    private final Holder<Attribute> attribute;
    private final ResourceLocation id;
    private double amount;
    private final AttributeModifier.Operation operation;
    private final EquipmentSlotGroup slot;

    public ItemModifierEntry(Holder<Attribute> attribute, ResourceLocation id, double amount, AttributeModifier.Operation operation, EquipmentSlotGroup slot) {
        this.attribute = attribute;
        this.id = id;
        this.amount = amount;
        this.operation = operation;
        this.slot = slot;
    }

    public boolean is(ResourceLocation resourceLocation) {
        return this.id.equals(resourceLocation);
    }

    public Holder<Attribute> attribute() {
        return this.attribute;
    }

    public ResourceLocation id() {
        return this.id;
    }

    public double amount() {
        return this.amount;
    }

    public AttributeModifier.Operation operation() {
        return this.operation;
    }

    public EquipmentSlotGroup slot() {
        return this.slot;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
